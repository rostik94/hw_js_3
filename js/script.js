import {CSS_COLOR_NAMES} from './colors.js';
import {data} from './data.js';

const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

canvas.width = 1400;
canvas.height = 1200;

const radius = (canvas.width - 200) / 4;
const centerX = (canvas.width - 200) / 2;
const centerY = canvas.height / 2;

const drawPieSlice = (ctx, centerX, centerY, radius, startAngle, endAngle, color) => {
    ctx.fillStyle = color;
    ctx.beginPath();
    ctx.moveTo(centerX, centerY);
    ctx.arc(centerX, centerY, radius, startAngle, endAngle);
    ctx.closePath();
    ctx.fill();
    ctx.stroke();
};

const addLabels = (ctx, centerX, centerY, radius, textName, color, textShift, startAngle, endAngle, item) => {
    ctx.fillStyle = '#333';
    ctx.beginPath();
    ctx.textAlign = 'left';
    ctx.font = '20px Helvetica, Calibri';
    const textPositionX = centerX + radius * 1.3;
    const textPositionY = centerY - radius + textShift;
    ctx.fillText(textName, textPositionX, textPositionY + 20);

    ctx.beginPath();
    ctx.strokeStyle = '#333';
    ctx.fillStyle = color;
    ctx.rect(textPositionX - 30, textPositionY, 20, 20);
    ctx.stroke();
    ctx.fill();

    ctx.beginPath();
    ctx.fillStyle = '#333';
    ctx.textAlign = 'center';
    ctx.font = '14px Helvetica, Calibri';
    const theta = (startAngle + endAngle) / 2;
    const deltaX = Math.cos(theta) * radius;
    const deltaY = Math.sin(theta) * radius;
    const piePercent = Math.round(item.population / population * 100 * 10) / 10 + '%';
    ctx.fillText(piePercent, deltaX * 1.3 + centerX, deltaY * 1.3 + centerY);

    ctx.beginPath();
    ctx.moveTo(deltaX + centerX, deltaY + centerY);
    ctx.lineTo(deltaX * 1.2 + centerX, deltaY * 1.2 + centerY);
    ctx.stroke();
    ctx.closePath();
};

const population = data.reduce((accum, item) => accum + item.population, 0);
const bigCountry = data.filter(item => item.population * 100 / population >= 1);
const smallCountry = data.filter(item => item.population * 100 / population <= 1).reduce((accum, item) => accum + item.population, 0);
const sortedBigCountry = bigCountry.sort((a, b) => b.population - a.population);
    
const diagramData = [...sortedBigCountry, {
    ID: 'OTH',
    country: 'Other',
    population: smallCountry
}];

diagramData.reduce(([startAngle, textShift], item, index) => {
    const sliceAngle = 2 * Math.PI * item.population / population;
    const endAngle = startAngle + sliceAngle;
    
    drawPieSlice(
        ctx,
        centerX,
        centerY,
        radius,
        startAngle,
        endAngle,
        CSS_COLOR_NAMES[index]
    );

    addLabels(
        ctx,
        centerX,
        centerY,
        radius,
        item.country,
        CSS_COLOR_NAMES[index],
        textShift,
        startAngle,
        endAngle,
        item
    );

    return [
        startAngle = endAngle,
        textShift += 30
    ];

}, [0, 0]);